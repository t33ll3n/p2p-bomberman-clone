flame.png
size: 240, 48
format: RGBA8888
filter: Linear,Linear
repeat: none
Flame_f00
  rotate: false
  xy: 0, 0
  size: 48, 48
  orig: 48, 48
  offset: 0, 0
  index: -1
Flame_f01
  rotate: false
  xy: 48, 0
  size: 48, 48
  orig: 48, 48
  offset: 0, 0
  index: -1
Flame_F02
  rotate: false
  xy: 96, 0
  size: 48, 48
  orig: 48, 48
  offset: 0, 0
  index: -1
Flame_F03
  rotate: false
  xy: 144, 0
  size: 48, 48
  orig: 48, 48
  offset: 0, 0
  index: -1
Flame_F04
  rotate: false
  xy: 192, 0
  size: 48, 48
  orig: 48, 48
  offset: 0, 0
  index: -1
