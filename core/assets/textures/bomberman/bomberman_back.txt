bomberman_back.png
size: 256, 256
format: RGBA8888
filter: Linear,Linear
repeat: none
Bman_B_f00
  rotate: false
  xy: 0, 0
  size: 64, 128
  orig: 64, 128
  offset: 0, 0
  index: -1
Bman_B_f01
  rotate: false
  xy: 64, 0
  size: 64, 128
  orig: 64, 128
  offset: 0, 0
  index: -1
Bman_B_f02
  rotate: false
  xy: 128, 0
  size: 64, 128
  orig: 64, 128
  offset: 0, 0
  index: -1
Bman_B_f03
  rotate: false
  xy: 192, 0
  size: 64, 128
  orig: 64, 128
  offset: 0, 0
  index: -1
Bman_B_f04
  rotate: false
  xy: 0, 128
  size: 64, 128
  orig: 64, 128
  offset: 0, 0
  index: -1
Bman_B_f05
  rotate: false
  xy: 64, 128
  size: 64, 128
  orig: 64, 128
  offset: 0, 0
  index: -1
Bman_B_f06
  rotate: false
  xy: 128, 128
  size: 64, 128
  orig: 64, 128
  offset: 0, 0
  index: -1
Bman_B_f07
  rotate: false
  xy: 192, 128
  size: 64, 128
  orig: 64, 128
  offset: 0, 0
  index: -1
