package network.util;

import commons.config.NetworkConstants;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.util.Enumeration;
import java.util.concurrent.ThreadLocalRandom;

public class LocalUtil {
    public static String getLocalAddress() {
        String ip = "";
        try {
            Enumeration<NetworkInterface> interfaces;
            interfaces = NetworkInterface.getNetworkInterfaces();
            while(interfaces.hasMoreElements()) {
                NetworkInterface iface = interfaces.nextElement();
                if (iface.isLoopback() || !iface.isUp()) {
                    continue;
                }
                Enumeration<InetAddress> addresses = iface.getInetAddresses();
                while(addresses.hasMoreElements()) {
                    InetAddress addr = addresses.nextElement();
                    ip = addr.getHostAddress();
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }
        return ip;
    }

    public static String getPublicAddress() {
        try {
            URL whatismyip = new URL("http://checkip.amazonaws.com");
            BufferedReader in = null;
            in = new BufferedReader(new InputStreamReader(
                    whatismyip.openStream()));
            String ip = in.readLine();
            if (in != null) {
                in.close();
            }
            return ip;
        } catch (Exception e) {
            System.out.println("Could not connect to amazonaws.com to acquire public ip!");
            return null;
        }
    }


    public boolean isPortInUse(int port) {
        try {
            (new Socket("127.0.0.1", port)).close();
            return true;
        }
        catch(IOException e) {
            return false;
        }
    }

}
