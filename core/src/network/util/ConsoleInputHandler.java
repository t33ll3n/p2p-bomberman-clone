package network.util;

import commons.enums.Action;
import commons.objects.Event;
import network.ClientCommunicator;
import network.models.RoomServer;

import java.util.Random;
import java.util.Scanner;

public class ConsoleInputHandler implements Runnable{

    private ClientCommunicator cc;

    public ConsoleInputHandler(ClientCommunicator cc) {
        this.cc = cc;
    }

    @Override
    public void run() {
        Scanner s = new Scanner(System.in);
        while (true) {
            String command = s.nextLine();
            if (command.startsWith("/")) {
                command = command.replaceFirst("/", "");
                String[] args = command.split(" ");
                if (args.length > 0) {
                    switch (args[0]) {
                        case "connect":
                            connect(args);
                            break;
                        case "disconnect":
                            cc.disconnect();
                            break;
                        case "lobby":
                            lobby(args);
                            break;

                        case "bc":
                            bc(args);
                            break;

                        default:
                            System.out.println("Unknown command");
                    }
                } else {
                    System.out.println("No command!");
                }

            } else {
                System.out.println("Commands should start with \"/\"");
            }
        }
    }

    private void connect(String[] args) {
        if (args[1].equals("lobby")) {
            cc.connectToLobbyServer();
        } else {
            cc.joinRoomByName(args[1]);
        }
    }

    private void lobby(String[] args) {
        if (args[1].equals("fetchRooms")) {
            cc.fetchRooms();
        } else if (args[1].equals("host")) {
            cc.hostGame();
        } else {
            System.out.println("Commands: /lobby <command>\n\tfetchRooms\t->\tfetch list of rooms\n\thost\t->host a room\t");
        }
    }

    private void bc(String[] args) {
        RoomServer rs = cc.getRoomServer();
        Event e = new Event(
                Action.valueOf(args[1]),
                cc.getName(),
                new Random().nextLong()
        );
        rs.broadcastEvent(e);
    }
}
