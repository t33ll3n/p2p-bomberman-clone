package network.models;

import network.enums.MessageEnums;
import network.util.CryptoUtil;

public class Message {
    private MessageEnums.Headers header;
    private MessageEnums.Type type;
    private long timestamp;
    private String body;
    private String signature;
    private String publicKey;

    public Message(){};

    public Message(MessageEnums.Headers header, long timestamp, String body, String signature, String publicKey) {
        this.header = header;
        this.timestamp = timestamp;
        this.body = body;
        this.signature = signature;
        this.publicKey = publicKey;
    }

    public boolean isValid() {
        return (
                this.header != null &&
                this.body != null &&
                this.signature != null &&
                this.publicKey != null
        );
    }

    public MessageEnums.Headers getHeader() {
        return header;
    }

    public void setHeader(MessageEnums.Headers header) {
        this.header = header;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public MessageEnums.Type getType() {
        return type;
    }

    public void setType(MessageEnums.Type type) {
        this.type = type;
    }

    public void pack() throws Exception {
        this.setTimestamp(System.currentTimeMillis());
        this.setPublicKey(CryptoUtil.getPublicKey());
        this.setSignature(CryptoUtil.sign(this.getBody()));
    }
}
