package network.models;

import commons.config.NetworkConstants;
import commons.objects.Event;
import network.ClientCommunicator;
import network.protocols.BroadcastEventProtocol;
import network.protocols.FetchPeersProtocol;
import network.util.LocalUtil;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class RoomServer implements Runnable {

    private static ExecutorService executor = Executors.newCachedThreadPool();

    private ServerSocket serverSocket;
    private ClientCommunicator cc;
    private Map<String, Client> members = new HashMap<>();
    private Room room;
    private Client host;

    public RoomServer(ClientCommunicator cc) throws IOException {
        this.serverSocket = new ServerSocket(NetworkConstants.ROOM_HOST_PORT);
        this.cc = cc;
        System.out.println("Game server waiting for client connection on " + LocalUtil.getLocalAddress() + ":" + NetworkConstants.ROOM_HOST_PORT + "...");
    }

    public RoomServer(Room room, ClientCommunicator cc) throws IOException {
        this.serverSocket = new ServerSocket(NetworkConstants.ROOM_HOST_PORT);
        this.cc = cc;
        this.room = room;
        System.out.println("Game server waiting for client connection on " + LocalUtil.getLocalAddress() + ":" + NetworkConstants.ROOM_HOST_PORT + "...");
    }

    @Override
    public void run() {
        System.out.println("Room opened and running on port " + NetworkConstants.ROOM_HOST_PORT);
        while (true) {
            Socket s;
            try {
                s = serverSocket.accept();
            } catch (IOException e) {
                System.out.println("Error accepting incoming socket connection: " + e.getMessage());
                continue;
            }
            System.out.println("New client connected...");
            Client client = null;
            try {
                client = new Client(s, cc);
            } catch (IOException e) {
                System.out.println("Error accepting incoming client communication: " + e.getMessage());
                continue;
            }
            this.executor.submit(client);
        }
    }

    public void discoverPeers(ArrayList<ClientData> peers) {
        for (ClientData potentialPeer : peers) {
            if (!this.members.containsKey(potentialPeer.getPrivateIp()) && !potentialPeer.getPrivateIp().equals(LocalUtil.getLocalAddress())) {
                System.out.println("Peer " + potentialPeer.getPrivateIp() + " not known! Initiating connection!");
                this.cc.connect(potentialPeer.getPrivateIp(), potentialPeer.getPort());
            }
        }
    }

    public void setHost(Client host) {
        this.host = host;
        this.members.put(host.getLocalIp(), host);
        new FetchPeersProtocol(this.cc).initiate(host);
    }

    public Map<String, Client> getMembers() {
        return this.members;
    }

    public void addMember(Client client) {
        this.members.put(client.getLocalIp(), client);
        new FetchPeersProtocol(this.cc).initiate(client);
    }

    public boolean broadcastEvent(Event event) {
        System.out.println("bla" + this.members.size());
        for (Client client : this.members.values()) {
            System.out.println("Broadcast target: " + client.getLocalIp());
            if (!client.getLocalIp().equals(LocalUtil.getLocalAddress())) {
                new BroadcastEventProtocol(this.cc).initiate(client, event);
            }
        }
        return true;
    }
}
