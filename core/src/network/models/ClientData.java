package network.models;

public class ClientData {
    private String privateIp;
    private int port;

    public ClientData(String privateIp, int port) {
        this.privateIp = privateIp;
        this.port = port;
    }

    public String getPrivateIp() {
        return privateIp;
    }

    public void setPrivateIp(String privateIp) {
        this.privateIp = privateIp;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
