package network.models;

public class Room {
    private String roomId;
    private String host;
    private String hostPublicIP;
    private String hostPrivateIP;
    private int port;


    public Room(String host, String hostPublicIP, String hostPrivateIP, int port) {
        this.roomId = "room" + (int)Math.round(Math.random()*10000);
        this.host = host;
        this.hostPublicIP = hostPublicIP;
        this.hostPrivateIP = hostPrivateIP;
        this.port = port;
    }

    public Room(String roomId, String hostName, String hostPublicIP, String hostPrivateIP, int port) {
        this.roomId = roomId;
        this.host = hostName;
        this.hostPublicIP = hostPublicIP;
        this.hostPrivateIP = hostPrivateIP;
        this.port = port;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getHostPublicIP() {
        return hostPublicIP;
    }

    public void setHostPublicIP(String hostPublicIP) {
        this.hostPublicIP = hostPublicIP;
    }

    public String getHostPrivateIP() {
        return hostPrivateIP;
    }

    public void setHostPrivateIP(String hostPrivateIP) {
        this.hostPrivateIP = hostPrivateIP;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String toString() {
        return "[" + getRoomId() + "] Host: " + getHost() + " | PubIP: " + getHostPublicIP() + " | privateIP: " + getHostPrivateIP() + " | port: " + getPort();
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
