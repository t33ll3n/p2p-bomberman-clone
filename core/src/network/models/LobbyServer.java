package network.models;

import network.ClientCommunicator;
import network.enums.MessageEnums;
import network.protocols.FetchRoomsProtocol;
import network.protocols.HostGameProtocol;
import network.protocols.Protocol;
import network.util.LocalUtil;

import java.io.*;
import java.net.Socket;

public class LobbyServer extends Client {

    public LobbyServer(Socket s, ClientCommunicator cc) throws IOException {
        super(s, cc);
    }

    public boolean hostGame(String hostName) {
        Protocol hostgameProtocol = new HostGameProtocol(super.getClientCommunicator());
        return hostgameProtocol.initiate(this);
    }

    public boolean fetchRooms(String me) {
        Protocol fetchRooms = new FetchRoomsProtocol(super.getClientCommunicator());
        return fetchRooms.initiate(this);
    }

    public Socket getSocket() {
        return super.socket;
    }

}
