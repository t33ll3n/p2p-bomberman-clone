package network.models;


import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import network.ClientCommunicator;
import network.protocols.Protocol;
import network.protocols.ProtocolMap;
import network.util.CryptoUtil;

import java.io.*;
import java.net.Socket;

public class Client implements Runnable {

    private String name;
    private String localIp;
    private String publicIp;
    private String port;

    protected Socket socket;
    private BufferedWriter out;
    private BufferedReader in;
    private Gson gson;
    private boolean keepAlive;
    private ClientCommunicator cc;

    public Client(Socket s, ClientCommunicator cc) throws IOException {
        this.cc = cc;
        this.socket = s;
        this.keepAlive = true;
        this.gson = new Gson();
        this.out = new BufferedWriter(new OutputStreamWriter(s.getOutputStream()));
        this.in = new BufferedReader(new InputStreamReader(s.getInputStream()));
    }

    @Override
    public void run() {
        String line;
        try {
            while (this.keepAlive && (line = this.in.readLine()) != null) {
                Message m = parseMessage(line);
                if (CryptoUtil.verify(m.getBody(), m.getSignature(), m.getPublicKey())) {
                    Protocol protocol = ProtocolMap.map(m);
                    protocol.digest(m, this);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JsonSyntaxException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean sendMessage(Message message) {
        try {
            message.pack();
            if (message.isValid()) {
                this.out.write(gson.toJson(message));
                this.out.newLine();
                this.out.flush();
                return true;
            } else {
                System.out.println("Message invalid!");
                return false;
            }

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e) {
            System.out.println("Error packing message");
            e.printStackTrace();
            return false;
        }
    }


    private Message parseMessage(String jsonString) {
        Message message = gson.fromJson(jsonString, Message.class);
        return message;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocalIp() {
        return localIp;
    }

    public void setLocalIp(String ip) {
        this.localIp = ip;
    }

    public String getPublicIp() {
        return publicIp;
    }

    public void setPublicIp(String publicIp) {
        this.publicIp = publicIp;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }


    protected ClientCommunicator getClientCommunicator() {
        return this.cc;
    }
}
