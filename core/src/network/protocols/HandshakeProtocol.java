package network.protocols;

import network.ClientCommunicator;
import network.enums.MessageEnums;
import network.models.Client;
import network.models.Message;
import network.util.LocalUtil;

public class HandshakeProtocol implements Protocol {

    private ClientCommunicator cc;

    public HandshakeProtocol(ClientCommunicator cc) {
        this.cc = cc;
    }

    @Override
    public boolean digest(Message message, Client client) {
        System.out.println("Handshake " + message.getType() + " msg received from " + client.getName() + "!");
        // extracts the required data from the message
        if (message.getType() != MessageEnums.Type.ACKNOWLEDGE) {
            handleMessage(message, client);
        }
        // setup the message variable to later be sent as a response
        Message handshakeMessage = null;
        try {
            // create a response message based on the type of the message received
            switch (message.getType()) {
                case REQUEST:
                    // respond to request
                    handshakeMessage = createResponseHandshakeMessage(client);
                    break;
                case RESPONSE:
                    // acknowledge response
                    handshakeMessage = createAcknowledgeHandshakeMessage(client);
                    break;
                case ACKNOWLEDGE:
                    // end of protocol
                    break;
            }
        } catch (Exception e) {
            System.out.println("Failed to respond with a handshake message for the client: " + client.getName());
            e.printStackTrace();
            return false;
        }
        // the received type was REQUEST or RESPONSE
        // if the handshakeMessage is null, the message type was ACK
        if (handshakeMessage != null) {
            boolean success = client.sendMessage(handshakeMessage);
            System.out.println("Handshake response sent: " + success);
            return success;
        } else {
            // do nothing if received an ACK type
            System.out.println("Client acknowledged the handshake!");
            return true;
        }
    }


    @Override
    public boolean initiate(Client client) {
        try {
            Message initialHandshakeMessage = createRequestHandshakeMessage(client);
            boolean success = client.sendMessage(initialHandshakeMessage);
            System.out.println("Handshake requested: " + success);
            return success;
        } catch (Exception e) {
            System.out.println("Failed to create a handshake message for the client: " + client.getName());
            e.printStackTrace();
            return false;
        }
    }


    private void handleMessage(Message message, Client client) {
        String[] data = message.getBody().split(" \\|\\| ");
        client.setName(data[0]);
        client.setLocalIp(data[1]);
        client.setPublicIp(data[2]);
        client.setPort(data[3]);
        if (this.cc.getRoomServer() != null) {
            this.cc.getRoomServer().addMember(client);
        }
    }

    private Message createAcknowledgeHandshakeMessage(Client client) throws Exception {
        Message message = new Message();
        message.setHeader(MessageEnums.Headers.HANDSHAKE);
        message.setType(MessageEnums.Type.ACKNOWLEDGE);
        message.setBody("");
        message.pack();
        return message;
    }


    private Message createRequestHandshakeMessage(Client client) throws Exception {
        Message message = new Message();
        message.setHeader(MessageEnums.Headers.HANDSHAKE);
        message.setType(MessageEnums.Type.REQUEST);
        message.setBody(cc.getName() + " || " + LocalUtil.getLocalAddress() + " || " + LocalUtil.getPublicAddress() + " || 4000" );
        message.pack();
        return message;
    }

    private Message createResponseHandshakeMessage(Client client) throws Exception {
        Message message = new Message();
        message.setHeader(MessageEnums.Headers.HANDSHAKE);
        message.setType(MessageEnums.Type.RESPONSE);
        message.setBody(cc.getName() + " || " + LocalUtil.getLocalAddress() + " || " + LocalUtil.getPublicAddress() + " || 4000" );
        message.pack();
        return message;
    }
}
