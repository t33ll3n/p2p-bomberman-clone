package network.protocols;

import network.ClientCommunicator;
import network.enums.MessageEnums;
import network.models.Client;
import network.models.Message;
import network.models.Room;

import java.util.ArrayList;


public class FetchRoomsProtocol implements Protocol{

    private ClientCommunicator cc;

    public FetchRoomsProtocol(ClientCommunicator clientCommunicator) {
        this.cc = clientCommunicator;
    }

    @Override
    public boolean digest(Message message, Client client) {
        System.out.println("Room discovery " + message.getType() + " message received from " + client.getName());
        if (message.getType() == MessageEnums.Type.RESPONSE) {
            handleMessage(message);
        }

        Message responseMessage = null;
        try {
            switch (message.getType()) {
                case REQUEST:
                    // TODO: local server host
                    responseMessage = createResponseFetchRoomsMessage();
                    break;
                case RESPONSE:
                    // acknowledge the request
                    responseMessage = createAcknowledgeFetchRoomsMessage();
                    break;
                case ACKNOWLEDGE:
                    // end of protocol
                    break;
            }
        } catch (Exception e) {
            System.out.println("Failed to create a HOST_GAME message!");
            e.printStackTrace();
            return false;
        }
        // respond to client
        if (responseMessage != null) {
            client.sendMessage(responseMessage);
        }
        return true;
    }

    @Override
    public boolean initiate(Client client) {
        try {
            Message message = createRequestFetchRoomsMessage();
            boolean success = client.sendMessage(message);
            System.out.println("Sent list of hosted rooms request message: " + success);
            return success;
        } catch (Exception e) {
            System.out.println("Failed to send message to fetch hosted rooms!");
            e.printStackTrace();
            return false;
        }
    }


    private void handleMessage(Message message) {
        Room[] rooms = unpackRooms(message.getBody());
        logRooms(rooms);
        cc.setRooms(rooms);
    }

    private Message createRequestFetchRoomsMessage() throws Exception {
        Message message = new Message();
        message.setHeader(MessageEnums.Headers.ROOM_DISCOVERY);
        message.setType(MessageEnums.Type.REQUEST);
        message.setBody("");
        message.pack();
        return message;
    }

    private Message createAcknowledgeFetchRoomsMessage() throws Exception {
        Message message = new Message();
        message.setHeader(MessageEnums.Headers.ROOM_DISCOVERY);
        message.setType(MessageEnums.Type.ACKNOWLEDGE);
        message.setBody("");
        message.pack();
        return message;
    }

    private Message createResponseFetchRoomsMessage() throws Exception {
        Message message = new Message();
        message.setHeader(MessageEnums.Headers.ROOM_DISCOVERY);
        message.setType(MessageEnums.Type.RESPONSE);
        message.setBody(packRooms(cc.getRooms()));
        message.pack();
        return message;
    }

    private String packRooms(Room[] rooms) {
        if (rooms.length == 0) {
            return "";
        }
        String roomString = "";
        String delimiter = " | ";
        for (Room room : rooms) {
            // separator of rooms
            if (!roomString.equals("")) {
                roomString += " || ";
            }
            // encode room
            roomString
                    += room.getRoomId() + delimiter
                    + room.getHost() + delimiter
                    + room.getHostPublicIP() + delimiter
                    + room.getHostPrivateIP() + delimiter
                    + room.getPort() + delimiter;
        }
        return roomString;
    }

    private Room[] unpackRooms(String fullRoomString) {
        String[] roomStrings = fullRoomString.split(" \\|\\| ");
        ArrayList<Room> rooms = new ArrayList<>();
        for (int i = 0 ; i < roomStrings.length ; i++ ) {
            String[] roomArgs = roomStrings[i].split(" \\| ");
            if (roomArgs.length != 5) {
                System.out.println("received invalid room: " + roomStrings[i]);
                continue;
            }
            rooms.add(new Room(roomArgs[0], roomArgs[1], roomArgs[2], roomArgs[3], Integer.parseInt(roomArgs[4])));
        }
        return rooms.toArray(new Room[rooms.size()]);
    }


    private void logRooms(Room[] rooms) {
        if (rooms == null) {
            System.out.println("No hosted rooms!");
        } else if (rooms.length == 0) {
            System.out.println("No hosted rooms!");
        } else {
            for (int i = 0 ; i < rooms.length ; i++) {
                System.out.println(rooms[i].toString());
            }
        }
    }
}
