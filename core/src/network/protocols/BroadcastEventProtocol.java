package network.protocols;

import commons.enums.Action;
import commons.objects.Event;
import network.ClientCommunicator;
import network.enums.MessageEnums;
import network.models.Client;
import network.models.Message;

public class BroadcastEventProtocol implements BroadcastProtocol {

    private ClientCommunicator cc;

    public BroadcastEventProtocol(ClientCommunicator clientCommunicator) {
        this.cc = clientCommunicator;
    }

    @Override
    public boolean digest(Message message, Client client) {
        System.out.println("Broadcast event message received from " + client.getName());
        // create a response message based on the type of the message received
        if (message.getType() == MessageEnums.Type.INFORM) {
            handleMessage(message, client);
        }
        Message responseMessage = null;
        try {
            switch (message.getType()) {
                case INFORM:
                    // acknowledge the request
                    responseMessage = createAcknowledgeEventMessage();
                    break;
                case ACKNOWLEDGE:
                    // end of protocol
                    break;
            }
        } catch (Exception e) {
            System.out.println("Failed to create a HOST_GAME message!");
            e.printStackTrace();
            return false;
        }
        // respond to client
        if (responseMessage != null) {
            client.sendMessage(responseMessage);
        }
        return true;
    }

    @Override
    public boolean initiate(Client client) {
        return false;
    }

    @Override
    public boolean initiate(Client client, Event event) {
        try {
            Message message = createEventMessage(event);
            boolean success = client.sendMessage(message);
            System.out.println("Sent event message: " + success);
            return success;
        } catch (Exception e) {
            System.out.println("Failed to send event message!");
            e.printStackTrace();
            return false;
        }
    }

    private Message createAcknowledgeEventMessage() throws Exception {
        Message message = new Message();
        message.setHeader(MessageEnums.Headers.BROADCAST_EVENT);
        message.setType(MessageEnums.Type.ACKNOWLEDGE);
        message.setBody("");
        message.pack();
        return message;
    }

    private Message createEventMessage(Event event) throws Exception {
        Message message = new Message();
        message.setHeader(MessageEnums.Headers.BROADCAST_EVENT);
        message.setType(MessageEnums.Type.INFORM);
        message.setBody(packEvent(event));
        message.pack();
        return message;
    }

    private String packEvent(Event event) {
        return event.toString();
    }

    private void handleMessage(Message message, Client client) {
        System.out.println("hey");

        this.cc.getValidator().handleEvent(new Event(message.getBody()), client);
    }
}
