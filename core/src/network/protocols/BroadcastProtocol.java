package network.protocols;

import commons.objects.Event;
import network.models.Client;

public interface BroadcastProtocol extends Protocol {
    boolean initiate(Client client, Event event);
}
