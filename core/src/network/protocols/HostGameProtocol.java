package network.protocols;

import network.ClientCommunicator;
import network.enums.MessageEnums;
import network.models.Client;
import network.models.Message;

public class HostGameProtocol implements Protocol{

    private ClientCommunicator cc;

    public HostGameProtocol(ClientCommunicator cc) {
        this.cc = cc;
    }

    @Override
    public boolean digest(Message message, Client client) {
        System.out.println("Host-game message received from " + client.getName());
        // create a response message based on the type of the message received
        Message responseMessage = null;
        try {
            switch (message.getType()) {
                case REQUEST:
                    // TODO: local server host
                    responseMessage = createResponseHostRoomMessage(false);
                    break;
                case RESPONSE:
                    // acknowledge the request
                    responseMessage = createAcknowlegeHostRoomMessage();
                    break;
                case ACKNOWLEDGE:
                    // end of protocol
                    break;
            }
        } catch (Exception e) {
            System.out.println("Failed to create a HOST_GAME message!");
            e.printStackTrace();
            return false;
        }
        // respond to client
        if (responseMessage != null) {
            client.sendMessage(responseMessage);
        }
        return true;
    }

    @Override
    public boolean initiate(Client client) {
        try {
            Message message = createRequestHostRoomMessage();
            boolean success = client.sendMessage(message);
            System.out.println("Sent host game request message: " + success);
            return success;
        } catch (Exception e) {
            System.out.println("Failed to send host game message!");
            e.printStackTrace();
            return false;
        }
    }

    private Message createRequestHostRoomMessage() throws Exception {
        Message message = new Message();
        message.setHeader(MessageEnums.Headers.GAME_HOST);
        message.setType(MessageEnums.Type.REQUEST);
        message.setBody("");
        message.pack();
        return message;
    }

    private Message createResponseHostRoomMessage(boolean roomHosted) throws Exception {
        Message message = new Message();
        message.setHeader(MessageEnums.Headers.GAME_HOST);
        message.setType(MessageEnums.Type.RESPONSE);
        message.setBody(String.valueOf(roomHosted));
        message.pack();
        return message;
    }

    private Message createAcknowlegeHostRoomMessage() throws Exception {
        Message message = new Message();
        message.setHeader(MessageEnums.Headers.GAME_HOST);
        message.setType(MessageEnums.Type.ACKNOWLEDGE);
        message.setBody("");
        message.pack();
        return message;
    }
}
