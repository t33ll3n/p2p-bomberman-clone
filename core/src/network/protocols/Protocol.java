package network.protocols;


import network.models.Client;
import network.models.Message;

public interface Protocol {

    boolean digest(Message message, Client client);

    boolean initiate(Client client);
}
