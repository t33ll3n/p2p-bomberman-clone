package network.protocols;


import network.ClientCommunicator;
import network.models.Message;

public class ProtocolMap {

    private static ClientCommunicator cc;

    public static void setCommunicator(ClientCommunicator comm) {
        cc = comm;
    }

    public static Protocol map(Message message) {
        System.out.println("Mapping recieved message...");
        switch (message.getHeader()) {
            case HANDSHAKE:
                return new HandshakeProtocol(cc);
            case GAME_HOST:
                return new HostGameProtocol(cc);
            case ROOM_DISCOVERY:
                return new FetchRoomsProtocol(cc);
            case PEER_DISCOVERY:
                return new FetchPeersProtocol(cc);
            case BROADCAST_EVENT:
                return new BroadcastEventProtocol(cc);
            default:
                return null;
        }
    }
}
