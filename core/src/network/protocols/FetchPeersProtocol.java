package network.protocols;

import commons.config.NetworkConstants;
import network.ClientCommunicator;
import network.enums.MessageEnums;
import network.models.Client;
import network.models.ClientData;
import network.models.Message;

import java.awt.image.AreaAveragingScaleFilter;
import java.util.ArrayList;

public class FetchPeersProtocol implements Protocol {

    private ClientCommunicator cc;

    public FetchPeersProtocol(ClientCommunicator clientCommunicator) {
        this.cc = clientCommunicator;
    }

    @Override
    public boolean digest(Message message, Client client) {
        System.out.println("Peer discovery " + message.getType() + " msg received from " + client.getName() + "!");
        // extracts the required data from the message
        if (message.getType() == MessageEnums.Type.RESPONSE) {
            handleMessage(message, client);
        }
        // setup the message variable to later be sent as a response
        Message peerMessage = null;
        try {
            // create a response message based on the type of the message received
            switch (message.getType()) {
                case REQUEST:
                    // respond to request
                    peerMessage = createResponsePeerDiscoveryMessage(client);
                    break;
                case RESPONSE:
                    // acknowledge response
                    peerMessage = createAcknowledgePeerDiscoveryMessage(client);
                    break;
                case ACKNOWLEDGE:
                    // end of protocol
                    break;
            }
        } catch (Exception e) {
            System.out.println("Failed to respond with a peer discovery message for the client: " + client.getName());
            e.printStackTrace();
            return false;
        }
        // the received type was REQUEST or RESPONSE
        // if the handshakeMessage is null, the message type was ACK
        if (peerMessage != null) {
            boolean success = client.sendMessage(peerMessage);
            System.out.println("Peer discovery response sent: " + success);
            return success;
        } else {
            // do nothing if received an ACK type
            System.out.println("Client " + client.getName() + " acknowledged peer response!");
            return true;
        }
    }

    @Override
    public boolean initiate(Client client) {
        try {
            Message initialPeerDiscoveryMessage = createRequestPeerDiscoveryMessage(client);
            boolean success = client.sendMessage(initialPeerDiscoveryMessage);
            System.out.println("Peer discovery requested: " + success);
            return success;
        } catch (Exception e) {
            System.out.println("Failed to create a peer discovery message for the client: " + client.getName());
            e.printStackTrace();
            return false;
        }
    }

    private Message createAcknowledgePeerDiscoveryMessage(Client client) throws Exception {
        Message message = new Message();
        message.setHeader(MessageEnums.Headers.PEER_DISCOVERY);
        message.setType(MessageEnums.Type.ACKNOWLEDGE);
        message.setBody("");
        message.pack();
        return message;
    }

    private Message createResponsePeerDiscoveryMessage(Client client) throws Exception {
        Message message = new Message();
        message.setHeader(MessageEnums.Headers.PEER_DISCOVERY);
        message.setType(MessageEnums.Type.RESPONSE);
        message.setBody(packPeers());
        message.pack();
        return message;
    }

    private Message createRequestPeerDiscoveryMessage(Client client) throws Exception {
        Message message = new Message();
        message.setHeader(MessageEnums.Headers.PEER_DISCOVERY);
        message.setType(MessageEnums.Type.REQUEST);
        message.setBody("");
        message.pack();
        return message;
    }

    private String packPeers() {
        System.out.println("Packing peers...");
        if (this.cc.getRoomServer() == null ) {
            return "";
        }
        String peers = "";
        for (Client client : this.cc.getRoomServer().getMembers().values()) {
            System.out.println("Parsing peer: " + client.getLocalIp());
            if (!peers.equals("")) {
                peers += " || ";
            }
            peers += client.getLocalIp() + " | " + NetworkConstants.ROOM_HOST_PORT;
        }
        System.out.println("Sending peers: " + peers);
        return peers;
    }

    private void handleMessage(Message message, Client client) {
        ArrayList<ClientData> peers = unpackPeers(message.getBody());
        cc.getRoomServer().discoverPeers(peers);
    }

    private ArrayList<ClientData> unpackPeers(String body) {
        String[] clientStrings = body.split(" \\|\\| ");
        ArrayList<ClientData> clients = new ArrayList<>();
        for (String clientString : clientStrings) {
            String[] clientStringData = clientString.split(" \\| ");
            if (clientStringData.length != 2) {
                System.out.println("Received invalid peer data!");
                continue;
            }
            clients.add(new ClientData(
                    clientStringData[0],
                    Integer.parseInt(clientStringData[1])
            ));
        }
        return clients;
    }
    
    
}
