package network.enums;

public class MessageEnums {

    public enum Headers {
        HANDSHAKE,
        PEER_DISCOVERY,
        HOLE_PUNCH,
        ROOM_DISCOVERY,
        GAME_HOST,
        BROADCAST_EVENT
    }

    public enum Type {
        REQUEST,
        RESPONSE,
        INFORM,
        ACKNOWLEDGE
    }
}
