package network;

import actionValidator.Validator;
import com.badlogic.gdx.maps.tiled.TiledMap;
import commons.config.NetworkConstants;
import commons.enums.Action;
import commons.interfaces.ActionValidator;
import commons.interfaces.Communicator;
import commons.objects.Event;
import network.holePunching.ClientA;
import network.models.Client;
import network.models.LobbyServer;
import network.models.Room;
import network.models.RoomServer;
import network.protocols.HandshakeProtocol;
import network.protocols.Protocol;
import network.protocols.ProtocolMap;
import network.util.ConsoleInputHandler;
import network.util.LocalUtil;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ClientCommunicator implements Communicator {

    private static ExecutorService executor = Executors.newCachedThreadPool();
    private LobbyServer lobbyServer;
    private Validator validator;

    private String me = "node" + (int) Math.round(Math.random() * 1000);
    private ArrayList<Room> rooms = new ArrayList<>();
    private RoomServer roomServer;

    public ClientCommunicator(String me) {
        // name of the client
        // this.me = me;
        // protocol map requires a reference to the currently active ClientCommunicator
        // it passes the reference to the protocols, so the protocol may communicate to other clients
        ProtocolMap.setCommunicator(this);
        this.executor.submit(new ConsoleInputHandler(this));
    }

    @Override
    public boolean connectToLobbyServer() {
        System.out.println("Connecting to lobby server...");
        // try to establish a connection to the lobby server
        // if the error is thrown, the function returns false
        try {
            // create the socket connection to the server
            Socket lobbyServerSoket = new Socket(
                    NetworkConstants.LOBBY_SERVER,
                    NetworkConstants.LOBBY_SERVER_PORT
            );


            // create the Client from the connection (init communications channels)
            this.lobbyServer = new LobbyServer(lobbyServerSoket, this);
            this.executor.submit(this.lobbyServer);
            System.out.println(
                    "Successfully connected to the lobby server! (" +
                            NetworkConstants.LOBBY_SERVER +
                            ", " +
                            NetworkConstants.LOBBY_SERVER_PORT +
                            ")"
            );
            // exchange name data via handshake protocol
            Protocol handshake = new HandshakeProtocol(this);
            return handshake.initiate(lobbyServer);
        } catch (IOException e) {
            System.out.println("Failed to connect to lobby server!");
            e.printStackTrace();
            return false;
        }
    }

    public Client connect(String ip, int port) {
        System.out.println("Connecting to " + ip + "...");
        // try to establish a connection to the server
        // if the error is thrown, the function returns false
        try {
            // create the socket connection to the server
            Socket socket = new Socket(ip, port);
            // create the Client from the connection (init communications channels)
            Client client = new Client(socket, this);
            this.executor.submit(client);
            System.out.println("Successfully connected to the client/server! (" + ip + ", " + port + ")");
            // exchange name data via handshake protocol
            Protocol handshake = new HandshakeProtocol(this);
            handshake.initiate(client);
            return client;
        } catch (IOException e) {
            System.out.println("Failed to connect to client/server!");
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean fetchRooms() {
        // check for connection to the lobby server
        if (this.lobbyServer == null) {
            return false;
        }
        return this.lobbyServer.fetchRooms(this.me);
    }

    @Override
    public Room[] getRooms() {
        return (Room[]) this.rooms.toArray(new Room[0]);
    }

    public void setRooms(Room[] rooms) {
        this.rooms = new ArrayList<>();
        if (rooms == null) {
            return;
        }
        if (rooms.length > 0) {
            for (Room r : rooms) {
                this.rooms.add(r);
            }
        }
    }

    public void setRooms(ArrayList<Room> rooms) {
        this.rooms = rooms;
    }

    @Override
    public boolean hostGame() {
        // check for connection to the lobby server

        try {
            this.roomServer = new RoomServer(this);
            this.executor.submit(this.roomServer);
            System.out.println("Game server opened!");
        } catch (IOException e) {
            System.out.println("Error running game server!");
            System.out.println(e.getMessage());
            return false;
        }
        if (this.lobbyServer == null) {
            return false;
        }
        return this.lobbyServer.hostGame(this.me);
    }

    @Override
    public boolean joinRoom(Room room) {
        if (this.roomServer != null) {
            System.out.println("Already in a room!");
        }
        try {

            this.roomServer = new RoomServer(room, this);
            this.executor.submit(this.roomServer);
            System.out.println("Game server opened!");
        } catch (IOException e) {
            System.out.println("Error running game server!");
            System.out.println(e.getMessage());
            return false;
        }
        Client host = connect(room.getHostPrivateIP(), room.getPort());
        roomServer.setHost(host);
        return true;
    }

    public boolean joinRoomByName(String name) {
        for (Room room : rooms) {
            if (room.getRoomId().equals(name)) {
                System.out.println("Attempting to join room " + name + " hosted by " + room.getHost() + "...");
                return joinRoom(room);
            }
        }
        System.out.println("No room with that id found!");
        return false;
    }

    @Override
    public boolean propagateEvent(Event event) {
        return this.getRoomServer().broadcastEvent(event);
    }

    @Override
    public boolean propagateStartGame(TiledMap map) {
        return true;
    }

    @Override
    public boolean propagateEndGame() {
        return true;
    }

    @Override
    public void setValidator(Validator validator) {
        this.validator = validator;
    }

    public String getName() {
        return this.me;
    }

    public void disconnect() {
        this.roomServer = null;
    }

    public RoomServer getRoomServer() {
        return this.roomServer;
    }

    public Validator getValidator() {
        return validator;
    }

    public void punch() {
        try {
            new ClientA(lobbyServer.getSocket().getInetAddress(), 4000, 12350);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}