package game.bomberman;


import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;

public class Assets {
    private Handler h;
    public AssetManager manager;

    public Assets(Handler h) {
        this.h = h;
        this.manager = new AssetManager();
        //manager.setLoader(TiledMap.class, new TmxMapLoader());

        load();
    }

    public void load() {
        // bomberman
        manager.load("textures/bomberman/bomberman_back.png", Texture.class);
        manager.load("textures/bomberman/bomberman_front.png", Texture.class);
        manager.load("textures/bomberman/bomberman_side.png", Texture.class);
        // creep
        manager.load("textures/creep/creep_front.png", Texture.class);
        manager.load("textures/creep/creep_back.png", Texture.class);
        manager.load("textures/creep/creep_side.png", Texture.class);

        // objects
        manager.load("textures/blocks/explodableBlock.png", Texture.class);
        manager.load("textures/flame.png", Texture.class);
        manager.load("textures/bomb.png", Texture.class);
        manager.load("textures/blocks/solidBlock.png", Texture.class);
        manager.load("textures/blocks/backgroundTile.png", Texture.class);
        manager.load("textures/blocks/portal.png", Texture.class);

        // map
        //manager.load("maps/map1.tmx", TiledMap.class);
        System.out.println("loaded");

    }
}
