package game.bomberman;

import actionValidator.Validator;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import commons.enums.Action;
import commons.objects.Event;

public class GameHandler extends Game {
	//SpriteBatch batch;
	//Texture img;
	private Handler h;
	private Validator validator;
	@Override
	public void create () {
		h = new Handler(this);
		//batch = new SpriteBatch();
		//img = new Texture("badlogic.jpg");
	}

	@Override
	public void render () {
		super.render();
	}

	@Override
	public void dispose () {
		h.dispose();
	}

	public void setValidator(Validator validator) {
		this.validator = validator;
	}

	public boolean handleEvent(Event event) {

		return false;
	}
}
