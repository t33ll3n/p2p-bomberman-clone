package game.bomberman.utils;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import game.bomberman.Handler;
import game.bomberman.Player;
import game.bomberman.enums.State;

public class InputManager implements InputProcessor {
    Handler h;
    public InputManager(Handler h) {
        this.h = h;

    }
    @Override
    public boolean keyDown(int keycode) {
        if(h.state == State.IN_GAME){
            return handleGameActions(keycode);
        }
        System.out.println(keycode);
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        System.out.println("keyUp");
        this.h.playScreen.player.setMoving(false);
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    public boolean handleGameActions(int keycode) {
        this.h.playScreen.player.setMoving(true);
        //move up
        if(keycode == Input.Keys.W) {
            h.playScreen.player.move("up");
        }
        // move down
        if(keycode == Input.Keys.S) {
            h.playScreen.player.move("down");
        }
        if(keycode == Input.Keys.A) {
            h.playScreen.player.move("left");
        }
        if(keycode == Input.Keys.D) {
            h.playScreen.player.move("right");
        }
        return false;
    }
}
