package game.bomberman;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import game.bomberman.enums.State;
import game.bomberman.screens.LoadingScreen;
import game.bomberman.screens.MenuScreen;
import game.bomberman.screens.PlayScreen;
import game.bomberman.utils.InputManager;

public class Handler {
    public GameHandler gameHandler;
    public Assets assets;
    public static int WIDTH = 800;
    public static int HEIGHT = 800;

    public SpriteBatch batch;
    public Viewport viewport;
    private OrthographicCamera camera;

    public PlayScreen playScreen;
    public MenuScreen menuScreen;

    public InputManager inputManager;

    public Enum state;

    public Handler(GameHandler gameHandler) {
        this.gameHandler = gameHandler;
        this.batch = new SpriteBatch();
        this.camera = new OrthographicCamera();
        this.viewport = new FitViewport(WIDTH, HEIGHT, camera);

        viewport.apply();
        camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);

        this.inputManager = new InputManager(this);
        Gdx.input.setInputProcessor(inputManager);

        this.assets = new Assets(this);
        setScreen(new LoadingScreen(this));

        this.state = State.MENU;

    }

    public void initScreens(){
        playScreen = new PlayScreen(this);
        menuScreen = new MenuScreen(this);

    }
    public void setScreen(Screen s) {
        gameHandler.setScreen(s);
    }
    public SpriteBatch getBatch(){
        return this.batch;
    }
    public OrthographicCamera getCamera(){
        return this.camera;
    }
    public Viewport getViewport(){
        return this.viewport;
    }
    public void dispose() {

    }
}
