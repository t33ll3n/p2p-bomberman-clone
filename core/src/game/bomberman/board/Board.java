package game.bomberman.board;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.utils.viewport.Viewport;
import game.bomberman.Handler;

public class Board {
    private Handler h;
    private TiledMap map;
    private Viewport viewport;
    private AssetManager manager;
    private OrthogonalTiledMapRenderer renderer;
    private OrthographicCamera camera;

    public Board(Handler h) {
        this.h = h;
        this.manager = h.assets.manager;
        this.camera = h.getCamera();
        this.viewport = h.getViewport();
        //manager.setLoader(TiledMap.class, new TmxMapLoader());
        map = new TmxMapLoader().load("maps/map1.tmx");

        //map = manager.get("maps/map1.tmx");
        renderer = new OrthogonalTiledMapRenderer(map);

    }

    public void render(SpriteBatch batch) {
        renderer.setView(camera);
        renderer.render();
    }
}
