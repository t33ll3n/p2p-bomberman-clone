package game.bomberman.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.Viewport;
import game.bomberman.Handler;

public class LoadingScreen  implements Screen {
    private Handler h;

    private SpriteBatch batch;
    private OrthographicCamera camera;
    private Viewport viewport;
    private boolean loading = true;

    public LoadingScreen(Handler h) {
        this.h = h;
        this.batch = h.getBatch();
        this.camera = h.getCamera();
        this.viewport = h.getViewport();
    }

    private void loadAssets() {
        h.assets.load();
        h.assets.manager.finishLoading();
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        camera.update();

        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        batch.end();
        if(loading) {
            loading = true;
            loadAssets();
            h.initScreens();
            h.setScreen(h.playScreen);
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
