package game.bomberman.screens;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.Viewport;
import game.bomberman.Handler;

public class MenuScreen implements Screen {
    private Handler h;
    private SpriteBatch batch;
    private OrthographicCamera camera;
    private Viewport viewport;

    public MenuScreen(Handler h) {
        this.h = h;
        this.batch = h.getBatch();
        this.camera = h.getCamera();
        this.viewport = h.getViewport();
    }
    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {

    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
        viewport.apply();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
