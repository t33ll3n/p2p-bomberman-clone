package game.bomberman.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.Viewport;
import game.bomberman.Handler;
import game.bomberman.Player;
import game.bomberman.board.Board;
import game.bomberman.enums.State;

public class PlayScreen implements Screen {
    private Handler h;
    private SpriteBatch batch;
    private OrthographicCamera camera;
    private Viewport viewport;
    private Texture img;
    public Player player;
    private Board board;
    public PlayScreen(Handler h) {
        this.h = h;
        this.h.state = State.IN_GAME;
        img = h.assets.manager.get("textures/bomberman/bomberman_back.png");
        this.batch = h.getBatch();
        this.camera = h.getCamera();
        this.viewport = h.getViewport();
        this.board = new Board(h);
        this.player = new Player(h, 0,0);
    }

    @Override
    public void show() {

    }
    public void update(float dt) {
        camera.update();
        player.update(dt);
    }
    @Override
    public void render(float delta) {
        update(delta);
        //textures drag without these 2 lines
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        //batch.draw(img,0,0);
       // board.render(batch);
        player.render(batch);

        batch.end();

    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
        viewport.apply();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
