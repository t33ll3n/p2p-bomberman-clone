package game.bomberman.objects;

import game.bomberman.Handler;

public interface Bomberman {

    public Bomb placeBomb(int x, int y);

    public int getID();

    public float getX();

    public float getY();

    public void getTile();
}
