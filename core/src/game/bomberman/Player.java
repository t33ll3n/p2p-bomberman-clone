package game.bomberman;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import game.bomberman.objects.Bomb;
import game.bomberman.objects.Bomberman;

public class Player implements Bomberman {
    private Handler h;
    private TextureAtlas backAtlas, frontAtlas, sideAtlas;
    private Animation backTex, frontTex, sideTex, tex;
    protected float elapsedTime = 0;
    public TextureRegion region;
    public float WIDTH, HEIGHT;
    public String orientation;
    public float x,y;
    private float speed;
    private boolean moving;

    public Player(Handler h, float x, float y) {
        this.h = h;
        backAtlas = new TextureAtlas(Gdx.files.internal("textures/bomberman/bomberman_back.txt"));
        backTex = new Animation(1/15f, backAtlas.getRegions());
        frontAtlas = new TextureAtlas(Gdx.files.internal("textures/bomberman/bomberman_front.txt"));
        frontTex = new Animation(1/15f, frontAtlas.getRegions());
        //side atlas is used for both sides
        sideAtlas = new TextureAtlas(Gdx.files.internal("textures/bomberman/bomberman_side.txt"));
        sideTex = new Animation(1/15f, sideAtlas.getRegions());
        tex = frontTex;

        this.orientation = "down";
        this.speed = 5;
        this.x = x;
        this.y = y;
        this.WIDTH = 100;
        this.HEIGHT = 100;
        this.moving = false;
    }

    public void update(float dt) {

        if(this.moving) {
            move(orientation);
        }
    }
    public void render(SpriteBatch batch) {
        elapsedTime += Gdx.graphics.getDeltaTime();
        region = (TextureRegion) tex.getKeyFrame(elapsedTime, true);
        batch.draw(region, orientation == "left" ? x + WIDTH : x, y, orientation == "left" ? - WIDTH : WIDTH, HEIGHT);
        //batch.draw(region, x, y);
    }

    @Override
    public Bomb placeBomb(int x, int y) {
        return null;
    }

    @Override
    public int getID() {
        return 0;
    }

    @Override
    public float getX() {
        return this.x;
    }

    @Override
    public float getY() {
        return this.y;
    }

    @Override
    public void getTile() {

    }
    public void move(String direction) {
        if(this.moving) {
            System.out.println("Started moving");
            if (direction == "right") {
                this.x += speed;
                this.tex = sideTex;
            }
            if (direction == "left") {
                this.x -= speed;
                this.tex = sideTex;
            }
            if (direction == "down") {
                this.y -= speed;
                this.tex = frontTex;
            }
            if (direction == "up") {
                this.y += speed;
                this.tex = backTex;
            }
            this.orientation = direction;
        }
        System.out.println("Stopped moving");
    }
    public void setMoving(boolean isMoving) {
        this.moving = isMoving;
    }
}
