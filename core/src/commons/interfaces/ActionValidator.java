/*
 * Interface class to interact with action validator class.
 * Action validator class implements this interface
 */
package commons.interfaces;

import commons.enums.Action;
import commons.objects.Event;
import network.models.Client;

public interface ActionValidator {
	
	//called by communication to let game know some remote events occur
    public boolean handleEvent(Event event, Client sender);
    
    //called by game to let propagate local events to other nodes
    public boolean propagateEvent(Action action, long tick);

    //Validate action
    public boolean validate(Action action, String name);


}
