/*
 * Interface to interact with static logger
 * Static logger class implements this interface
 */
package commons.interfaces;

public interface StaticLogger {

	//Write an event
    public void log(String log);

    //Get whole game log
    public String getLog();
}
