package commons.interfaces;

import actionValidator.Validator;
import com.badlogic.gdx.maps.tiled.TiledMap;
import commons.enums.Action;
import commons.objects.Event;
import network.models.Room;

public interface Communicator {

    // dependency injection for the Validator object
    void setValidator(Validator validator);

    // initiates the connection to the lobby server if possible
    boolean connectToLobbyServer();

    // fetches hosted game lobbies from the web server
    boolean fetchRooms();

    Room[] getRooms();

    // hosts a game on the lobby server
    boolean hostGame();

    // connects to the specified lobby
    // initiates the handshakes between connected players
    // returns boolean whether it was a success
    boolean joinRoom(Room lobby);

    // given a game event, the module will broadcast it to the players in the same game
    boolean propagateEvent(Event event);

    // propagates the 'start game' event to all the players in the lobby
    boolean propagateStartGame(TiledMap map);

    // propagates the 'end game' event to all the players in the game
    boolean propagateEndGame();
}
