package commons.config;

public class NetworkConstants {
    public static final String LOBBY_SERVER = "188.230.255.105";
    public static final int LOBBY_SERVER_PORT = 4000;
    public static final String CRYPTO_KEY_STORE_PATH = "./crypto/";
    public static final int ROOM_HOST_PORT = 12000;
}
