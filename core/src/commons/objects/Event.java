package commons.objects;

import commons.enums.Action;

import java.util.HashMap;
import java.util.Map;

public class Event {
    /*
    action performed by some player
     */
    private Action action;
    /*
    name of the player that performed the action
     */
    private String initiator;
    /*
    key: value pairs that enable to send more complex data than just action
     */
    private Map<String, String> data = new HashMap<>();
    /*
    tick in which the event was made
    */
    private long tick;

    public Event(String event) {
        String[] args = event.split(" \\|\\|\\| ");
        this.action = Action.valueOf(args[0]);
        this.initiator = args[1];
        this.tick = Long.parseLong(args[2]);
        if (args.length > 3) {
            for (int i = 3 ; i < args.length ; i++) {
                String[] pair = args[i].split(" \\|\\| ");
                this.data.put(pair[0], pair[1]);
                System.out.println("Data pair: " + pair[0] + " -> " + pair[1]);
            }
        }

    }

    public Event(Action action, String name, long tick) {
        this.action = action;
        this.initiator = name;
        this.tick = tick;
    }

    public String toString() {
        String delimiter = " ||| ";
        String event = "";
        event += this.action.toString() + delimiter;
        event += initiator + delimiter;
        event += tick + delimiter;
        if (!this.data.keySet().isEmpty()) {
            for (String key : this.data.keySet()) {
                event += key + " || " + this.data.get(key) + delimiter;
            }
        }
        return event;
    }



    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public String getInitiator() {
        return initiator;
    }

    public void setInitiator(String initiator) {
        this.initiator = initiator;
    }

    public Map<String, String> getData() {
        return data;
    }

    public void setData(Map<String, String> data) {
        this.data = data;
    }

    public void addToData(String key, String value) {
        this.data.put(key, value);
    }

    public void deleteFromData(String key) {
        this.data.remove(key);
    }

    public long getTick() {
        return tick;
    }

    public void setTick(long tick) {
        this.tick = tick;
    }
}
