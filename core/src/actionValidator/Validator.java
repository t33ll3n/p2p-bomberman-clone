package actionValidator;

import commons.enums.Action;
import commons.objects.Event;
import game.bomberman.GameHandler;
import network.ClientCommunicator;
import network.models.Client;

import java.util.Random;

public class Validator implements commons.interfaces.ActionValidator {
    ClientCommunicator client;
    GameHandler handler;

    public Validator(ClientCommunicator client, GameHandler handler) {
        this.client = client;
        this.handler = handler;
        this.client.setValidator(this);
        this.handler.setValidator(this);
    }

    @Override
    public boolean handleEvent(Event event, Client sender) {
        //Dummy function
        System.out.println("[ " + event.getInitiator() + " ] " + event.getTick() + ": " + event.getAction());
        return handler.handleEvent(event);
    }

    @Override
    public boolean propagateEvent(Action action, long tick) {
        //Dummy function
        Event e = new Event(
                action,
                client.getName(),
                tick
        );
        e.addToData("bla", "blabal");
        return client.propagateEvent(e);
    }

    @Override
    public boolean validate(Action action, String name) {
        return false;
    }
}
