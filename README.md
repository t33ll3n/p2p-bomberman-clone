# p2p-bomberman-clone

Peer-2-peer Bomberman clone


Gradle template for libgdx projects that works on the CLI, Eclipse and Intellij IDEA. This tempate will eventually
replace the libgdx Setup-UI currently in use. To get started **[Download the template](https://github.com/libgdx/libgdx-gradle-template/archive/master.zip)**
or fork and clone this repository.

### A word about Gradle
[Gradle](http://www.gradle.org/) is a nifty build and dependency managment system. A build system allows you to easily
compile and package (potentially different versions of) your project. A dependency management system allows
you to declaratively specify 3rd party libraries, which the dependency management system will
pull in automatically for you. No need to put Jar files or native libraries in your project tree.


You do not need to install Gradle manually, this template uses the [Gradle wrapper](http://www.gradle.org/docs/current/userguide/gradle_wrapper.html)
which takes care of that for you transparently. The first time you invoke gradle on the command line,
the Gradle wrapper will automatically download and install Gradle into your home directory.


To use this template you do not need to know Gradle necessarily, pretty much everything is taken 
care of for you.

### A word about Source Control
The contents of this repository are everything you need to commit to your source control repository.
Do not commit Eclipse or IntelliJ Idea projec files, especially if other people are working on the
same project. Instead, everyone just downloads this IDE-unaware project, then uses Gradle to generate
local IDE project files.

Also make sure to commit the gradlew, gradlew.bat files and the gradle/ folder. This way, nobody needs
to manually install Gradle on their system.

### Project Structure
This template has the following structure (ignoring some directores and files in the Android project for brevity).

    build.gradle                 <- main Gradle build file, add dependencies here
    settings.gradle              <- definition of the sub-modules for core, desktop, Android
    
    core/                        <- Core project
       build.gradle              <- Gradle build file for core project, no need to touch this
       src/                      <- All your game's source code goes here!
       
    desktop/                     <- Desktop project
       build.gradle              <- Gradle build file for desktop project, no need to touch this
       src/                      <- the desktop project's source code, contains launcher class
       
    android/                     <- Android project
       AndroidManifest.xml       <- Android specific configuration
       build.gradle              <- Gradle build file for Android project, DON'T EVER TOUCH THIS!
       assets/                   <- contains all your graphics, audio, etc. files, shared with desktop
       res/                      <- contains icons of your app and other resources
       src/                      <- the Android project's source code, contains launcher class


#### Running the desktop project
To run the desktop project issue this gradle command:

    ./gradlew desktop:run
    
#### Packaging the desktop project
To create a ZIP distribution including shell scripts to start your app, issue this gradle command:

    ./gradlew desktop:distZip
    
This will create a ZIP file in the folder desktop/build/distributions, ready to be send to testers.
    

### Eclipse Usage
You can let Gradle generate Eclipse projects for your application easily:

    ./gradlew eclipse
    
Once Gradle is done, **delete the .project file in the root directory of the project**. This is a 
little glitch that will get resolved shorty.

Now you can import the projects into Eclipse

  1. File -> Import ... -> General -> Existing projects into Workspace
  2. Click 'Browse', select the root folder of the project. You should see the core, android and desktop project.
  3. Make sure the three projects are checked, then click 'OK'

#### Running/Debugging in Eclipse
To run/debug the desktop project: 
  1. Right click the desktop project in the package viewer
  2. Select either 'Run As' or 'Debug As'
  3. Select 'Java Application'
  
To run/debug the Android project:
  1. Right click the Android project in the package viewer
  2. Select either 'Run As' or 'Debug As'
  3. Select 'Android Application'
  
### Intellij Idea Usage
You can let Gradle generate Intellij Idea project for your application easily:

    ./gradlew idea
    
Once Gradle is done, you can open the projects in Intellij Idea:

  1. File -> Open
  2. Select the .ipr file in the root of the project, Click 'OK'
  
You'll need to set the Android SDK on the Android module before continuing:

  1. File -> Project Structure ...
  2. Select 'modules' in the left side panel
  3. Select the 'android' module in the modules panel
  4. Set 'Module SDK' to the Android SDK you configured for IntelliJ Idea
  
#### Running/Debugging in Intellij Idea
To run/debug the desktop project, first create a run configuration:

  1. Run -> Edit Configurations
  2. Click the plus ('+') in the top left corner, select 'Application'
  3. Set the Name of the configuration, e.g. 'Desktop'
  4. Set the Main Class to 'DesktopLauncher', by clicking on the button on the left of the field
  5. Set the Working Directory to $root/android/assets, by clicking on the button on the left of the field
  6. Set 'Use classpath of module' to 'desktop'
  7. Click 'OK'
  
To run/debug the desktop project, just select the run configuration you just created
and then either click the green play button, or the green play button with the bug.

#### Running with Docker

Once the `jar` file of the game has been created with the gradle `dist` task, we can build the docker image by running
```
docker build .
```
in the root directory. This will generate the docker container. Replace the `a` value in the `runDocker.sh` with the name of the container created.
Once the name has beed speficid, the container can be run with
```
./runDocker.sh
```