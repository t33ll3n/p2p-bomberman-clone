FROM ubuntu:18.04

ENV USER dev

RUN apt-get update
RUN apt-get install openjdk-8-jre -y
RUN apt-get install docker.io -y
RUN apt-get install pulseaudio -y
RUN apt-get install libxcursor-dev -y
RUN apt-get update && \
    apt-get install --no-install-recommends -y libasound2-plugins libopenal1 zenity libopenal-dev libsndfile1-dev && \
    rm -rf /var/lib/apt/lists/* && \
    echo "drivers=pulse" >> /etc/openal/alsoft.conf
RUN apt-get update && apt-get -y install sudo
RUN apt-get install x11-xserver-utils -y
RUN useradd --create-home --home-dir $HOME $USER
RUN usermod -a -G audio $USER
RUN chown -R $USER:$USER $HOME/

WORKDIR /
COPY ./JAR /app
USER $USER

EXPOSE 5000
CMD java -jar /app/desktop-1.0.jar
