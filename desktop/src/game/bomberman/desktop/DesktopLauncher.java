package game.bomberman.desktop;

import actionValidator.Validator;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import game.bomberman.GameHandler;
import network.ClientCommunicator;
import network.util.CryptoUtil;

public class DesktopLauncher {
	public static void main (String[] arg) {
		System.out.println("babalbalbalbal");
		CryptoUtil.init();
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		GameHandler handler = new GameHandler();
		ClientCommunicator cc = new ClientCommunicator("my name");
		Validator validator = new Validator(cc, handler);

		cc.connectToLobbyServer();

		new LwjglApplication(handler, config);
	}
}
